import type { Products } from '../interfaces';

export const arrayPagination = (arr: Array<Products>, size: number) => {
  const paginated: Array<Array<Products>> = arr.reduce((acc: any, val, i) => {
    const idx = Math.floor(i / size) as any;
    const page: any[] = acc[idx] || (acc[idx] = []);
    page.push(val);

    return acc;
  }, []);

  return {
    meta: {
      totalLength: arr.length,
      totalPages: paginated.length
    },
    data: paginated,
  };
};

export const formatMoney = (amount: string | number, decimalCount = 2, decimal = '.', thousands = ',') => {
  decimalCount = Math.abs(decimalCount);
  decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
  const parserNum1 = parseInt((amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))).toString();
  const parserCond = parserNum1.length > 3 ? parserNum1.length % 3 : 0;
  return (
    (parserCond ? parserNum1.substr(0, parserCond) + thousands : '') +
    parserNum1.substr(parserCond).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
    (decimalCount
      ? decimal +
        Math.abs(Number(amount) - Number(parserNum1))
          .toFixed(decimalCount)
          .slice(2)
      : '')
  );
};
