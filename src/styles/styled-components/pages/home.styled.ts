import styled from 'styled-components';
import { theme } from '../../utilities/colors';

const ContainerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: auto;
  margin-bottom: 100px;
`;

const ContainerContent = styled.div`
  display: flex;
  flex-direction: column;
  padding: 50px 0;
  width: 100%;
  height: 100%;
`;

export const Wrapper = {
  Container: ContainerWrapper,
  Content: ContainerContent,
};

const ProductContainer = styled.ul`
  display: grid;
  gap: 1rem;
  grid-template-columns: repeat(4, minmax(0, 1fr));
  margin: 0;
  padding: 0;
  list-style: none;
  margin-top: 50px;
  @media (max-width: 1199px) {
    grid-template-columns: repeat(3, minmax(0, 1fr));
  }
  @media (max-width: 991px) {
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }
  @media (max-width: 767px) {
    grid-template-columns: repeat(1, minmax(0, 1fr));
  }
`;

const ProductCard = styled.div`
  display: flex;
  width: 100%;
  height: 325px;
  border-radius: 16px;
  background: ${theme.gray[300]};
  transition: transform 200ms ease-out 200ms;
  &:hover {
    transform: translate(0, -5px);
    z-index: 1;

    &::after {
      opacity: 0.5;
    }
  }
  &::after {
    border-radius: 0.9375em;
    box-shadow: 0px 6px 9px #0000004b;
    content: '';
    height: 100%;
    left: 0;
    opacity: 0;
    pointer-events: none;
    position: absolute;
    top: 0;
    transition: opacity 200ms ease-out 200ms;
    width: 100%;
    z-index: -1;
  }
`;

const ProductCardContent = styled.ul`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: auto;
  padding: 30px 10px;
  list-style: none;
  position: relative;
`;

const ProducCardtDetails = styled.li`
  display: flex;
  flex-direction: row;
  height: 65px;
  border-bottom: 1px solid ${theme.gray[500]};
  &:not(:first-child) {
    height: 40px;
    align-items: center;
  }
  &:last-child {
    border-bottom: none;
    height: auto;
    position: absolute;
    bottom: 30px;
    right: 10px;
  }
`;

export const Product = {
  Container: ProductContainer,
  Card: ProductCard,
  CardContent: ProductCardContent,
  CardDetails: ProducCardtDetails,
};

const ButtonNavigation = styled.button`
  width: auto;
  padding: 0 20px;
  cursor: pointer;
  font-weight: 700;
  background-color: ${theme.gray[500]};
  color: ${theme.white};
  border: 1px solid ${theme.white};
  border-radius: 3px;
  transition: 300ms;
  outline: none;
  outline-color: transparent;
  &.cart {
    width: 20px;
    height: 20px;
    padding: 0;
    margin-top: 5px;
    border-radius: 9999px;
  }
  &:hover {
    background-color: ${theme.white};
    color: ${theme.gray[700]};
    &.cart {
      background-color: ${theme.gray[500]};
      color: ${theme.white};
      opacity: 0.7;
    }
  }
  &:focus {
    outline: none;
    outline-color: transparent;
  }
  &:disabled {
    pointer-events: none;
    opacity: 0.6;
  }
`;

export const NavigationInput = styled.input`
  width: 300px;
  height: 50px;
  padding: 0 20px;
  background-color: ${theme.gray[500]};
  border: 1px solid ${theme.white};
  outline: none;
  outline-color: transparent;
  font-weight: 500;
  margin-left: 20px;
  border-radius: 10px;
  letter-spacing: 0.78px;
  ::-webkit-input-placeholder {
    color: ${theme.darkTeal[900]};
  }
  ::-moz-placeholder {
    color: ${theme.darkTeal[900]};
  }
  :-ms-input-placeholder {
    color: ${theme.darkTeal[900]};
  }
  :-moz-placeholder {
    color: ${theme.darkTeal[900]};
  }
`;

export const Navigation = {
  Button: ButtonNavigation,
  Input: NavigationInput,
};