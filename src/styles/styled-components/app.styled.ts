import styled from 'styled-components';
import { theme } from '../utilities/colors';

const ContainerWrapper = styled.div`
  height: 100%;
  position: relative;
  z-index: 2;
`;

const MainWrapper = styled.main`
  position: relative;
  overflow-y: auto;
  overflow-x: hidden;
  scrollbar-width: thin;
  scrollbar-color: ${theme.gray[700]} transparent;
  scrollbar-width: thin;
  ::-webkit-scrollbar {
    width: 5px;
    background-color: transparent;
    &-track {
      background-color: transparent;
    }
    &-thumb {
      background-color: ${theme.gray[700]};
      border-radius: 5px;
    }
  }
`;

export const Wrapper = {
  Container: ContainerWrapper,
  Main: MainWrapper,
};

const HeaderWrapper = styled.header`
  background-color: ${theme.gray[700]};
  color: rgb(14, 30, 43);
  position: relative;
  z-index: 10;
`;

const HeaderBody = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const HeaderContent = styled.div`
  display: flex;
  flex-direction: row;
  padding: 20px 0;
  width: 100%;
`;

const HeaderButton = styled.button`
  width: auto;
  height: auto;
  padding: 0;
  margin: 0;
  background: transparent;
  border: none;
  outline: none;
  outline-color: transparent;
  transition: 300ms;
  position: relative;
  &:focus {
    outline: none;
    outline-color: transparent;
  }
  &:hover {
    opacity: 0.7;
    cursor: pointer;
  }
  .icon-cart {
    font-size: 24px;
    color: ${theme.white};
  }
  .counter {
    position: absolute;
    width: 20px;
    height: 20px;
    border-radius: 99999px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: ${theme.red[500]};
    color: ${theme.white};
    font-weight: 700;
    top: -7px;
    right: -15px;
  }
`;

export const Header = {
  Wrapper: HeaderWrapper,
  Body: HeaderBody,
  Content: HeaderContent,
  Button: HeaderButton,
};