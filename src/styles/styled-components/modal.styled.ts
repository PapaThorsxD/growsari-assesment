import styled, { keyframes } from 'styled-components';
import { theme } from '../utilities/colors';

const enter = keyframes`
  0% {
    opacity: 0;
    transform: translateY(-20px);
  }
  100% {
    opacity: 1;
    transform: translateY(0);
  }
`;

const ModalWrapper = styled.div`
  align-items: center;
  background-color: rgba(0,0,0,0.4);
  bottom: 0;
  display: flex;
  left: 0;
  padding: 10px;
  position: fixed;
  right: 0;
  top: 0;
  transition: opacity 0.2s ease;
  z-index: 100;
`;

const ModalBody = styled.div`
  border-radius: 5px;
  animation: ${enter} 0.3s forwards;
  background-color: ${theme.white};
  margin: auto;
  max-width: 100%;
  padding: 35px 70px;
  position: relative;
  width: 770px;
  @media(max-width: 767px) {
    padding: 30px 50px;
  }
  .btn__close {
    cursor: pointer;
    opacity: 0.5;
    transition: opacity 0.2s ease;
    outline: none;
    &:hover {
      opacity: 1;
    }
  }
`;

const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 20px;
  width: 100%;
  border-top: 1px solid ${theme.gray[500]};
  padding-top: 20px;
  overflow: scroll;
  scrollbar-width: thin;
  scrollbar-color: ${theme.gray[700]} transparent;
  scrollbar-width: thin;
  ::-webkit-scrollbar {
    width: 5px;
    background-color: transparent;
    &-track {
      background-color: transparent;
    }
    &-thumb {
      background-color: ${theme.gray[700]};
      border-radius: 5px;
    }
  }
`;

const ModalTable = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  .items {
    margin-bottom: 5px;
    &-content {
      border: 1px solid ${theme.gray[500]};
      padding: 10px 20px;
      border-radius: 10px;
      position: relative;
      .remove {
        position: absolute;
        right: 0;
        top: -4px;
        width: 20px;
        height: 20px;
        font-weight: 700;
        background-color: ${theme.red[500]};
        color: ${theme.white};
        display: flex;
        font-size: 12px;
        justify-content: center;
        align-items: center;
        border-radius: 99999px;
        transition: 300ms;
        &:hover {
          cursor: pointer;
          opacity: 0.7;
        }
      }
    }
  }
`;

const ModalFooter = styled.footer`
  display: flex;
  width: 100%;
  height: 70px;
  border-top: 1px solid ${theme.gray[500]};
`;

export const Modal = {
  Wrapper: ModalWrapper,
  Body: ModalBody,
  Content: ModalContent,
  Table: ModalTable,
  Footer: ModalFooter,
};
