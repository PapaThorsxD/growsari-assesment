import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Modal } from '../styles/styled-components/modal.styled';
import { Direction, Text } from '../styles/styled-components/global.styled';
import clsx from 'clsx';
import { formatMoney } from '../utils';
import { Navigation } from '../styles/styled-components/pages/home.styled';
import { RootState } from '../redux/store';
import { addToCart } from '../redux/appSlice';

type Props = {
  modalVisible: boolean;
  setModalVisible: React.Dispatch<React.SetStateAction<boolean>>,
}

const ModalComponent: React.FC<Props> = ({ modalVisible, setModalVisible }) => {
  const dispatch = useDispatch();
  const cart = useSelector((state: RootState) => state.systemApp.cart);
  const [totalPrice, setTotalPrice] = React.useState<number>(0);

  const removeItems = (key: number) => {
    if (cart && cart[key].id) {
      const cartDetails = JSON.parse(JSON.stringify(cart));
      cartDetails.splice(key, 1);
      dispatch(addToCart([...cartDetails]));
    }
  }

  const setQuantity = (type: 'add' | 'sub', key: number) => {
    if (cart && cart[key].id) {
      if (!!cart[key]?.id) {
        const cartDetails = JSON.parse(JSON.stringify(cart));
        switch (type) {
          case 'add':
            cartDetails[key].quantity = cartDetails[key].quantity + 1;
            dispatch(addToCart([...cartDetails]))
            break;
          case 'sub':
            if (cartDetails[key]?.quantity > 1) {
              cartDetails[key].quantity = cartDetails[key].quantity - 1;
              dispatch(addToCart([...cartDetails]))
            }
            break;
        }
      }
    }
  };

  const onKeyDown = React.useCallback((e: KeyboardEvent) => {
    if (e.code === 'Escape') {
      setModalVisible(false);
    }
  }, [])

  React.useEffect(() => {
    if (modalVisible) {
      window.addEventListener('keydown', onKeyDown);
    }
    return () => {
      window.removeEventListener('keydown', onKeyDown);
    };
  }, [modalVisible, onKeyDown]);

  React.useEffect(() => {
    if (cart && cart?.length > 0) {
      let getPrice = 0;
      cart.forEach((item) => {
        getPrice += (item.price * item.quantity)
      });

      setTotalPrice(getPrice);
    }
  }, [cart]);

  return (
    <Modal.Wrapper>
      <Modal.Body>
        <Direction.Row className="justify-content-between">
          <Text.Title>Product Cart</Text.Title>
          <span
            className="btn__close"
            tabIndex={-1} role="button"
            onClick={() => setModalVisible(false)}>
              Close
            </span>
        </Direction.Row>
        <Modal.Content
          className={clsx(
            { 'h-100 justify-content-center align-items-center': Number(cart?.length || 0) === 0 },
            { 'h-500' : Number(cart?.length || 0) > 0}
          )}
        >
          {Number(cart?.length || 0) === 0 ? <Text.Title>Cart is Empty</Text.Title> : (
            <Modal.Table>
              {cart?.map((item, key) => (
                <Direction.Col key={item.id}  className="items">
                  <div className="items-content">
                    <div className="remove" role="button" onClick={() => removeItems(key)}>x</div>
                    <Direction.Row>
                      <Text.Title className="heading line-32 flex-1">Name:</Text.Title>
                      <Text.Title className="heading line-32 flex-2">{item.display_name}</Text.Title>
                    </Direction.Row>
                    <Direction.Row>
                      <Text.Title className="heading line-32 flex-1">Category:</Text.Title>
                      <Text.SubTitle className="heading line-32 flex-2">{item.category}</Text.SubTitle>
                    </Direction.Row>
                    <Direction.Row>
                      <Text.Title className="heading line-32 flex-1">Brand:</Text.Title>
                      <Text.SubTitle className="heading line-32 flex-2">{item.brand}</Text.SubTitle>
                    </Direction.Row>
                    <Direction.Row>
                      <Text.Title className="heading line-32 flex-1">Price:</Text.Title>
                      <Text.SubTitle className="heading line-32 flex-2">{`₱ ${formatMoney(item.price)}`}</Text.SubTitle>
                    </Direction.Row>
                    <Direction.Row>
                      <Text.Title className="heading line-32 flex-1">Qty:</Text.Title>
                      <Direction.Row className="flex-2">
                        <Navigation.Button
                          type="button"
                          className="cart"
                          onClick={() => setQuantity('sub', key)}
                        >
                          -
                        </Navigation.Button>
                        <Text.SubTitle className="heading line-32 ">{item.quantity}</Text.SubTitle>
                        <Navigation.Button
                          type="button"
                          className="cart"
                          onClick={() => setQuantity('add', key)}
                        >
                          +
                        </Navigation.Button>
                      </Direction.Row>
                    </Direction.Row>
                  </div>
                </Direction.Col>
              ))}
            </Modal.Table>
          )}
        </Modal.Content>
        <Modal.Footer>
          <Direction.Row className="w-100 justify-content-between">
            <Text.Title className="heading">Total Price</Text.Title>
            <Text.Title className="heading">{`₱ ${formatMoney(totalPrice)}`}</Text.Title>
          </Direction.Row>
        </Modal.Footer>
      </Modal.Body>
    </Modal.Wrapper>
  );
};

export default ModalComponent;