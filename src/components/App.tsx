import React from 'react';
import { useSelector } from 'react-redux';
import { Wrapper, Header } from '../styles/styled-components/app.styled';
import { Direction, Text } from '../styles/styled-components/global.styled';
import Modal from './Modal';
import { ReactComponent as IconCart }  from '../styles/icons/icon-cart.svg';
import { RootState } from '../redux/store';

const App: React.FC = ({ children }) => {
  const cart = useSelector((state: RootState) => state.systemApp.cart);

  const [modalVisible, setModalVisible] = React.useState<boolean>(false);

  return (
    <Wrapper.Container>
      {modalVisible && <Modal modalVisible={modalVisible} setModalVisible={setModalVisible}/>}
      <Header.Wrapper>
        <Header.Body>
          <Direction.Row className="container justify-content-between align-items-center">
            <Header.Content className="align-center">
              <Text.Title className="light">Cart Exam</Text.Title>
            </Header.Content>
            <Header.Button type="button" onClick={() => setModalVisible(true)}>
              {cart?.length ? <div className="counter">{cart.length}</div> : null}
              <IconCart className="icon icon-cart" />
            </Header.Button>
          </Direction.Row>
        </Header.Body>
      </Header.Wrapper>
      <Wrapper.Main>
        {children}
      </Wrapper.Main>
    </Wrapper.Container>
  );
};

export default App;
