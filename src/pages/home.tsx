import React from 'react';
import swal from 'sweetalert';
import { Wrapper, Product, Navigation } from '../styles/styled-components/pages/home.styled';
import { Direction, Text } from '../styles/styled-components/global.styled';
import productList from '../products.json';
import { arrayPagination, formatMoney } from '../utils';
import { useDispatch, useSelector } from 'react-redux';
import type { Products } from '../interfaces';
import { addToCart } from '../redux/appSlice';
import { RootState } from '../redux/store';

const PAGE_SIZE = 60;
const PRODUCTS: Array<Products> = productList;

const Home: React.FC = () => {
  const dispatch = useDispatch();
  const cart = useSelector((state: RootState) => state.systemApp.cart);

  const [filteredData, setFilteredData] = React.useState<Array<Products>>();
  const [products, setProducts] = React.useState<Array<Products>>();
  const [activePage, setActivePage] = React.useState<number>(0);
  const [meta, setMeta] = React.useState<{ totalLength: number; totalPages: number }>();
  const [pageCount, setPageCount] = React.useState<{ current: number; tableCount: number }>();
  const [searchText, setSearchText] = React.useState<string>('');

  const onSearchChange = (e: any) => {
    const filteredProducts = PRODUCTS.filter((product: Products) => 
      product.display_name.toLowerCase().includes(e.target.value?.toLowerCase()) ,
    );
    setSearchText(e.target.value);
    setFilteredData(filteredProducts);
  };

  const pushToCart = (prod: Products) => {
    if (prod.id && cart) {
      const checkCart = cart.find((val) => val.id === prod.id);
      let message = '';
      if (!!checkCart?.id) {
        const value = JSON.parse(JSON.stringify(checkCart));
        value.quantity = checkCart.quantity + 1;
        const removeExist = cart.filter((val) => val.id !== prod.id);
        message = 'Product successfully added a quantity to cart';
        dispatch(addToCart([...removeExist, { ...value }]))
      } else {
        message = 'Product successfully added to cart';
        dispatch(addToCart([...cart, { ...prod, quantity: 1 }]))
      }
      swal({
        title: 'Success!',
        text: message,
        icon: 'success',
      });
    }
  };
  
  React.useEffect(() => {
    const forProducts = searchText || (filteredData?.length || 0) > 0 ? (filteredData || []) : PRODUCTS;
    const paginatedProducts = arrayPagination(forProducts.slice().splice(0, forProducts.length), PAGE_SIZE);
    setMeta(paginatedProducts.meta);
    setProducts(paginatedProducts.data[activePage]);
  }, [activePage, filteredData]);

  React.useEffect(() => {
    if (meta?.totalLength !== undefined && products !== undefined) {
      const pageNumber = activePage + 1;

      const currentNumber = Number(meta.totalLength) > 0
        ? (pageNumber === 1
          ? pageNumber
          : Number(pageNumber * PAGE_SIZE - PAGE_SIZE) + 1)
        : 0;
      const tableNumber = Number(pageNumber) === meta.totalPages ? (meta.totalPages - 1) 
        * PAGE_SIZE + Number(products.length || 1) : Number(pageNumber) * Number(products.length || 1);
      setPageCount({ current: currentNumber, tableCount: tableNumber });
    }
  }, [meta, activePage, products]);

  return (
    <Wrapper.Container className="container">
      <Wrapper.Content>
        <Direction.Row className="justify-content-between">
          <Text.Title className="light text-uppercase">Products</Text.Title>
          <Direction.Row className="align-items-center">
            <Navigation.Button disabled={!(activePage > 0)} type="button" onClick={() => activePage > 0 ? setActivePage(activePage - 1) : null}>{'<'}</Navigation.Button>
            <Text.SubTitle className="light mlr-20">
              {`${pageCount?.current || 0} to ${pageCount?.tableCount || 0} of ${meta?.totalLength}`}
            </Text.SubTitle>
            <Navigation.Button disabled={!(activePage < (Number(meta?.totalPages || 0) - 1))} type="button" onClick={() => activePage < (Number(meta?.totalPages || 0) - 1) ? setActivePage(activePage + 1) : null}>{'>'}</Navigation.Button>
            <Navigation.Input type="text" placeholder="Search Products" onChange={onSearchChange} />
          </Direction.Row>
        </Direction.Row>
        <Product.Container>
          {products?.map((product) => (
            <Product.Card key={product.id}>
              <Product.CardContent>
                <Product.CardDetails>
                  <Text.Title className="heading flex-1">Name: </Text.Title>
                  <Text.Title className="heading flex-2">{product.display_name.length > 40 ? `${product.display_name.substring(0, 40)}...` : product.display_name}</Text.Title>
                </Product.CardDetails>
                <Product.CardDetails>
                  <Text.Title className="heading flex-1">Category: </Text.Title>
                  <Text.Title className="heading flex-2">{product.category}</Text.Title>
                </Product.CardDetails>
                <Product.CardDetails>
                  <Text.Title className="heading flex-1">Brand: </Text.Title>
                  <Text.Title className="heading flex-2">{product.brand}</Text.Title>
                </Product.CardDetails>
                <Product.CardDetails>
                  <Text.Title className="heading flex-1">Price: </Text.Title>
                  <Text.Title className="heading flex-2">{`₱ ${formatMoney(product.price)}`}</Text.Title>
                </Product.CardDetails>
                <Product.CardDetails className="justify-content-end">
                  <Navigation.Button type="button" onClick={() => pushToCart(product)}>Add to cart</Navigation.Button>
                </Product.CardDetails>
              </Product.CardContent>
            </Product.Card>
          ))}
        </Product.Container>
      </Wrapper.Content>
    </Wrapper.Container>
  );
};

export default Home;