import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Products } from '../interfaces';

interface Cart extends Products {
  quantity: number;
};

export type InitialState = {
  cart?: Array<Cart>;
};

const initialState: InitialState = {
  cart: [],
};

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    addToCart(state, action: PayloadAction<Array<Cart>>) {
      if (state.cart) {
        state.cart = action.payload;
      }
    },
  },
});

export const { addToCart } = appSlice.actions;

export default appSlice.reducer;
