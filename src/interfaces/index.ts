export type Products = {
  id: number;
  display_name: string;
  barcode: number | string;
  price: number;
  brand: number | string;
  category: string;
};
